﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountCountFood
{
    public class ModelProduct
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public int id { get; set; }
        /// <summary>
        /// Дата добавления
        /// </summary>
        public DateTime dateCreate { get; set; }
        /// <summary>
        /// Наименование
        /// </summary>
        public string name { get; set; }
        /// <summary>
        /// Белок
        /// </summary>
        public int proteins { get; set; }
        /// <summary>
        /// Жиры
        /// </summary>
        public int fats { get; set; }
        /// <summary>
        /// Углеводы
        /// </summary>
        public int carbohydrates { get; set; }
        /// <summary>
        /// Калории
        /// </summary>
        public int kcal { get; set; }

        /// <summary>
        /// Модель продукта
        /// </summary>
        public ModelProduct()
        {

        }

        /// <summary>
        /// Модель продукта
        /// </summary>
        /// <param name="id">Идентификатор</param>
        /// <param name="dateCreate">Дата добавления</param>
        /// <param name="name">Наименование</param>
        /// <param name="proteins">Белки</param>
        /// <param name="fats">Жиры</param>
        /// <param name="carbohydrates">Углеводы</param>
        /// <param name="kcal">Калории</param>
        public ModelProduct(int id, DateTime dateCreate, string name, int proteins, int fats, int carbohydrates, int kcal)
        {
            this.id = id;
            this.dateCreate = dateCreate;
            this.name = name;
            this.proteins = proteins;
            this.fats = fats;
            this.carbohydrates = carbohydrates;
            this.kcal = kcal;
        }
    }
}
