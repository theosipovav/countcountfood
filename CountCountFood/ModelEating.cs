﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountCountFood
{

    public class ModelEating
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public int id;
        /// <summary>
        /// Дата создания
        /// </summary>
        public DateTime dateCreate;
        /// <summary>
        /// Идентификатор продукта
        /// </summary>
        public int productId;
        /// <summary>
        /// Вес продукта
        /// </summary>
        public int mass;

        public int type; 

        public ModelEating()
        {

        }
        public ModelEating (int id, DateTime dateCreate, int productId, int mass, int type)
        {
            this.id = id;
            this.dateCreate = dateCreate;
            this.productId = productId;
            this.mass = mass;
            this.type = type;
        }


    }
}
