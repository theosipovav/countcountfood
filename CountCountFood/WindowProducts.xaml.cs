﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CountCountFood
{
    /// <summary>
    /// Логика взаимодействия для WindowProducts.xaml
    /// </summary>
    public partial class WindowProducts : Window
    {


        /// <summary>
        /// Строка подключения к файлу базы данных Access
        /// </summary>
        public string connectionString { get; set; }
        /// <summary>
        /// Соединение с базой данных
        /// </summary>
        private OleDbConnection connection { get; set; }

        /// <summary>
        /// Коллекция загруженных продуктов
        /// </summary>
        public List<ModelProduct> listProducts;

        public WindowProducts(string connectionString)
        {
            this.connectionString = connectionString;
            InitializeComponent();

            updateData();
        }

        /// <summary>
        /// обновить данные 
        /// </summary>
        void updateData()
        {
            listProducts = new List<ModelProduct>();
            try
            {
                using (connection = new OleDbConnection(this.connectionString))
                {
                    connection.Open();
                    string sqlString;
                    OleDbCommand command;
                    OleDbDataReader reader;
                    sqlString = "SELECT products.id, products.date_create, products.name, products.proteins, products.fats, products.carbohydrates, products.kcal FROM products;";
                    command = new OleDbCommand(sqlString, connection);
                    reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        ModelProduct product = new ModelProduct();
                        product.id = reader.GetInt32(0);
                        product.dateCreate = reader.GetDateTime(1);
                        product.name = reader.GetString(2);
                        product.proteins = reader.GetInt32(3);
                        product.fats = reader.GetInt32(4);
                        product.carbohydrates = reader.GetInt32(5);
                        product.kcal = reader.GetInt32(6);
                        listProducts.Add(product);
                    }
                    connection.Close();
                }
                DataGirdProducts.ItemsSource = listProducts;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

        } 

        
        private void ButtonAddProduct_Click(object sender, RoutedEventArgs e)
        {
            int id = 0;
            DateTime dateCreate = new DateTime();
            string name = nameTextBox.Text;
            int proteins = 0;
            if (!Int32.TryParse(proteinsTextBox.Text, out proteins) || proteinsTextBox.Text == "")
            {
                MessageBox.Show("Некорректное значение поля <Белки>", "Внимание", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            int fats = 0;
            if (!Int32.TryParse(fatsTextBox.Text, out fats) || fatsTextBox.Text == "")
            {
                MessageBox.Show("Некорректное значение поля <Жиры>", "Внимание", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            int carbohydrates = 0;
            if (!Int32.TryParse(carbohydratesTextBox.Text, out carbohydrates) || carbohydratesTextBox.Text == "")
            {
                MessageBox.Show("Некорректное значение поля <Углеводы>", "Внимание", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            int kcal = 0;
            if (!Int32.TryParse(kcalTextBox.Text, out carbohydrates) || kcalTextBox.Text == "")
            {
                MessageBox.Show("Некорректное значение поля <Калории>", "Внимание", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            ModelProduct product = new ModelProduct(0, dateCreate, name, proteins, fats, carbohydrates, kcal);

            try
            {
                using (connection = new OleDbConnection(this.connectionString))
                {
                    connection.Open();
                    string sqlString = String.Format("INSERT INTO products(date_create,name,proteins,fats,carbohydrates,kcal) VALUES ('{0}','{1}','{2}','{3}','{4}','{5}')", DateTime.Now.ToString("dd.MM.yyyy"), product.name, product.proteins, product.fats, product.carbohydrates, product.kcal);
                    OleDbCommand command = new OleDbCommand(sqlString, connection);
                    int result = command.ExecuteNonQuery();
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }


            updateData();
        }

        /// <summary>
        /// 
        /// </summary>
        private void ButtonAddDelete_Click(object sender, RoutedEventArgs e)
        {
            var rowIndex = DataGirdProducts.SelectedIndex;
            var row = (DataGridRow)DataGirdProducts.ItemContainerGenerator.ContainerFromIndex(rowIndex);
            ModelProduct product = row.Item as ModelProduct;
            
        }

        /// <summary>
        /// Загрузка окна
        /// </summary>
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

            System.Windows.Data.CollectionViewSource modelProductsViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("modelProductsViewSource")));
            // Загрузите данные, установив свойство CollectionViewSource.Source:
            // modelProductsViewSource.Source = [универсальный источник данных]
        }

        private void ButtonCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
