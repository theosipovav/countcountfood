﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CountCountFood
{
    /// <summary>
    /// Логика взаимодействия для WindowCalc.xaml
    /// </summary>
    public partial class WindowCalc : Window
    {


        /// <summary>
        /// Строка подключения к файлу базы данных Access
        /// </summary>
        public string connectionString { get; set; }
        /// <summary>
        /// Соединение с базой данных
        /// </summary>
        private OleDbConnection connection { get; set; }
        public ModelConfig config;

        public WindowCalc(string connectionString, ModelConfig config)
        {
            this.connectionString = connectionString;
            this.config = config;
            InitializeComponent();
            ComboBoxPhysicalActivity.SelectedIndex = 1;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonCalc_Click(object sender, RoutedEventArgs e)
        {
            double targetDown = 0;
            double target = 0;
            double targetUp = 0;
            int mass = 0;
            if (Int32.TryParse(TextBoxMass.Text, out mass))
            {
                if (mass < 0)
                {
                    MessageBox.Show("Поле <Вес> постое или заполнено некорректно", "Внимание!", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
            }
            int age = 0;
            if (Int32.TryParse(TextBoxAge.Text, out age))
            {
                if (age < 0)
                {
                    MessageBox.Show("Поле <Возраст> постое или заполнено некорректно", "Внимание!", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
            }
            int height = 0;
            if (Int32.TryParse(TextBoxHeight.Text, out height))
            {
                if (height < 0)
                {
                    MessageBox.Show("Поле <Рост> постое или заполнено некорректно", "Внимание!", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
            }

            double mode = 1;
            if (ComboBoxPhysicalActivity.Text == "Основной обмен") { mode = 1; }
            if (ComboBoxPhysicalActivity.Text == "Минимум/отсутствие физической нагрузки") { mode = 1.2; }
            if (ComboBoxPhysicalActivity.Text == "3 раза в неделю") { mode = 1.375; }
            if (ComboBoxPhysicalActivity.Text == "5 раз в неделю") { mode = 1.4625; }
            if (ComboBoxPhysicalActivity.Text == "5 раз в неделю (интенсивно)") { mode = 1.55; }
            if (ComboBoxPhysicalActivity.Text == "Каждый день") { mode = 1.6375; }
            if (ComboBoxPhysicalActivity.Text == "Каждый день интенсивно или два раза в день") { mode = 1.725; }
            if (ComboBoxPhysicalActivity.Text == "Ежедневная физическая нагрузка+физическая работа") { mode = 1.9; }

            if (RadioButtonF1.IsChecked == true)
            {
                // Формула Миффлина-Сан Жеора
                if (RadioButtonMan.IsChecked == true && RadioButtonWoman.IsChecked == false)
                {
                    target = (10 * mass + 6.25 + height - 5 * age + 5) * mode;
                }
                else
                {
                    target = (10 * mass + 6.25 + height - 5 * age - 161) * mode;
                }
            }
            if (RadioButtonF2.IsChecked == true)
            {
                // Формула Харриса-Бенедикта
                if (RadioButtonMan.IsChecked == true && RadioButtonWoman.IsChecked == false)
                {
                    target = 66.5 + mass * 13.75 + height * 5.003 - 6.775 * age;
                }
                else
                {
                    target = 655.1 + (9.653 * mass) + height * 1.85 - 4.676 * age;
                }
            }
            targetDown = (int)(target + (target * 0.1));
            targetUp = (int)(target - (target * 0.1));
            LabelTargetDown.Content = targetDown.ToString();
            LabelTarget.Content = ((int)target).ToString();
            LabelTargetUp.Content = targetUp.ToString();

            try
            {
                using (this.connection = new OleDbConnection(this.connectionString))
                {
                    this.connection.Open();
                    string sqlString = String.Format("INSERT INTO config(age,mass,height,mode,target_down,target,target_up,date_create) VALUES ('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}')", age, mass, height, ComboBoxPhysicalActivity.Text, targetDown, target, targetUp, DateTime.Now.ToString("dd.MM.yyyy"));
                    OleDbCommand command = new OleDbCommand(sqlString, connection);
                    int result = command.ExecuteNonQuery();
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ComboBoxPhysicalActivity_SourceUpdated(object sender, DataTransferEventArgs e)
        {

        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            LabelTargetDown.Content = config.targetDown.ToString();
            LabelTarget.Content = config.target.ToString();
            LabelTargetUp.Content = config.targetUp.ToString();
            TextBoxAge.Text = config.age.ToString();
            TextBoxMass.Text = config.mass.ToString();
            TextBoxHeight.Text = config.height.ToString();


        }

        private void ButtonCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
