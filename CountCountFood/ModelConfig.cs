﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountCountFood
{
    public class ModelConfig
    {
        public int age { get; set; }
        public int mass { get; set; }
        public int height { get; set; }
        public string mode { get; set; }
        public int targetDown { get; set; }
        public int target { get; set; }
        public int targetUp { get; set; }
        public ModelConfig()
        {

        }
    }
}
