﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CountCountFood
{
    /// <summary>
    /// Логика взаимодействия для WindowAddEating.xaml
    /// </summary>
    public partial class WindowAddEating : Window
    {
        /// <summary>
        /// Строка подключения к файлу базы данных Access
        /// </summary>
        public string connectionString { get; set; }
        /// <summary>
        /// Соединение с базой данных
        /// </summary>
        private OleDbConnection connection { get; set; }
        List<ModelProduct> listProducts;

        int type = 1;

        public WindowAddEating(string connectionString,List<ModelProduct> listProducts, int type = 1)
        {
            this.connectionString = connectionString;
            this.listProducts = listProducts;
            this.type = type;
            InitializeComponent();
            switch (type)
            {
                case 1:
                    LabelTitle.Content = "Завтрак";
                    break;
                case 2:
                    LabelTitle.Content = "Обед";
                    break;
                case 3:
                    LabelTitle.Content = "Ужин";
                    break;
                case 4:
                    LabelTitle.Content = "Перекус";
                    break;
                default:
                    break;
            }
        }


        private void ButtonCreate_Click(object sender, RoutedEventArgs e)
        {
            int mass = 0;
            if (!Int32.TryParse(TextBoxMass.Text, out mass))
            {
                MessageBox.Show("Поле <Вес> постое или заполнено некорректно", "Внимание!", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            int rowIndex = -1;
            rowIndex = DataGirdProducts.SelectedIndex;
            if (rowIndex < 0)
            {
                MessageBox.Show("Продукт не выбран", "Внимание!", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            DataGridRow row = (DataGridRow)DataGirdProducts.ItemContainerGenerator.ContainerFromIndex(rowIndex);
            ModelProduct product = row.Item as ModelProduct;

            try
            {

                using (this.connection = new OleDbConnection(this.connectionString))
                {
                    this.connection.Open();
                    string sqlString = String.Format("INSERT INTO eating(date_create,product_id,mass,type) VALUES ('{0}','{1}','{2}','{3}')",  DateTime.Now.ToString("dd.MM.yyyy"), product.id, mass, this.type);
                    OleDbCommand command = new OleDbCommand(sqlString, connection);
                    int result = command.ExecuteNonQuery();
                    connection.Close();
                }
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }



        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            DataGirdProducts.ItemsSource = listProducts;
            
        }

        /// <summary>
        /// Фильтр
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TextBoxFilter_TextChanged(object sender, TextChangedEventArgs e)
        {
            DataGirdProducts.ItemsSource = listProducts.Where(x => x.name.Contains(TextBoxFilter.Text));
        }

        private void ButtonCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
