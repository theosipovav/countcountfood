﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CountCountFood
{
    /// <summary>
    /// Логика взаимодействия для WindowConnect.xaml
    /// </summary>
    public partial class WindowConnect : Window
    {
        /// <summary>
        /// Флаг подключения
        /// </summary>
        public bool isConnection;

        /// <summary>
        /// Строка подключения к файлу базы данных Access
        /// </summary>
        public string connectionString { get; set; }
        public WindowConnect()
        {
            InitializeComponent();
            TextBoxProvider.Text = "Provider=Microsoft.ACE.OLEDB.16.0";
            isConnection = false;
        }


        private void ButtonCancel_Click(object sender, RoutedEventArgs e)
        {
            this.isConnection = false;
            this.Close();
        }

        private void ButtonOpenFile_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.OpenFileDialog dialog = new System.Windows.Forms.OpenFileDialog();
            dialog.Multiselect = false;
            // mdb
            dialog.Filter = "База данных Access (*.accdb;*.mdb)|*.accdb;*.mdb|Все файлы (*.*)|*.*";
            if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                TextBoxDatabase.Text = dialog.FileName;

            }

        }

        private void ButtonNext_Click(object sender, RoutedEventArgs e)
        {
            if (TextBoxProvider.Text == "")
            {
                MessageBox.Show("Поле \"Провайдер подключения\" не должно быть пустым", "Внимание!", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            if (!File.Exists(TextBoxDatabase.Text))
            {
                MessageBox.Show("Выберите файл базы данных Access", "Внимание!", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }


            connectionString = String.Format("Provider=Microsoft.ACE.OLEDB.16.0;Data Source={0}", TextBoxDatabase.Text);
            using (OleDbConnection connection = new OleDbConnection(connectionString))
            {
                try
                {
                    connection.Open();
                    connection.Close();
                    this.isConnection = true;
                    this.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }

        }
    }
}
