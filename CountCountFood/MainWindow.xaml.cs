﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CountCountFood
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        /// <summary>
        /// Строка подключения к файлу базы данных Access
        /// </summary>
        public string connectionString { get; set; }
        /// <summary>
        /// Соединение с базой данных
        /// </summary>
        private OleDbConnection connection { get; set; }

        public ModelConfig config;
        public List<ModelEating> listEating;
        public List<ModelProduct> listProducts;


        public MainWindow()
        {



            WindowConnect windowConnect = new WindowConnect();
            windowConnect.ShowDialog();
            if (windowConnect.isConnection)
            {
                this.connectionString = windowConnect.connectionString;
                InitializeComponent();
            }
            else
            {
                Environment.Exit(0);
            }
        }

        private void ButtonProducts_Click(object sender, RoutedEventArgs e)
        {
            WindowProducts windowProducts = new WindowProducts(this.connectionString);
            windowProducts.ShowDialog();
            updateData();
        }

        /// <summary>
        /// Добавить завтрак
        /// </summary>
        private void ButtonAddBreakfast_Click(object sender, RoutedEventArgs e)
        {
            WindowAddEating windowAddEating = new WindowAddEating(this.connectionString, this.listProducts, 1);
            windowAddEating.ShowDialog();
            updateData();
        }

        /// <summary>
        /// Добавить обед
        /// </summary>
        private void ButtonAddDinner_Click(object sender, RoutedEventArgs e)
        {
            WindowAddEating windowAddEating = new WindowAddEating(this.connectionString, this.listProducts, 2);
            windowAddEating.ShowDialog();
            updateData();
        }

        /// <summary>
        /// Добавить ужин
        /// </summary>
        private void ButtonAddSupper_Click(object sender, RoutedEventArgs e)
        {
            WindowAddEating windowAddEating = new WindowAddEating(this.connectionString, this.listProducts, 3);
            windowAddEating.ShowDialog();
            updateData();
        }

        /// <summary>
        /// Добавить перекус
        /// </summary>
        private void ButtonAddSnack_Click(object sender, RoutedEventArgs e)
        {
            WindowAddEating windowAddEating = new WindowAddEating(this.connectionString, this.listProducts, 4);
            windowAddEating.ShowDialog();
            updateData();
        }

        private void ButtonCalc_Click(object sender, RoutedEventArgs e)
        {
            WindowCalc windowCalc = new WindowCalc(this.connectionString, this.config);
            windowCalc.ShowDialog();
            updateData();
        }
        /// <summary>
        /// Загрузка окна
        /// </summary>
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            updateData();
        }

        /// <summary>
        /// Обновить данные
        /// </summary>
        private void updateData()
        {
            LabelNow.Content = DateTime.Now.ToString("dd.MM.yyyy");
            int a = 0;
            int b = 0;
            int c = 0;
            int breakfast = 0;
            int dinner = 0;
            int supper = 0;
            int snack = 0;
            try
            {
                this.config = new ModelConfig();
                this.listProducts = new List<ModelProduct>();
                this.listEating = new List<ModelEating>();
                using (connection = new OleDbConnection(this.connectionString))
                {
                    connection.Open();
                    OleDbCommand command;
                    OleDbDataReader reader;
                    string sqlString = "SELECT TOP 1 config.age, config.mass, config.height, config.mode, config.target_down, config.target, config.target_up FROM config ORDER BY config.date_create DESC;";
                    command = new OleDbCommand(sqlString, connection);
                    reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        config.age = reader.GetInt32(0);
                        config.mass = reader.GetInt32(1);
                        config.height = reader.GetInt32(2);
                        config.mode = reader.GetString(3);
                        config.targetDown = reader.GetInt32(4);
                        config.target = reader.GetInt32(5);
                        config.targetUp = reader.GetInt32(6);
                        break;
                    }
                    sqlString = "SELECT products.id, products.date_create, products.name, products.proteins, products.fats, products.carbohydrates, products.kcal FROM products;";
                    command = new OleDbCommand(sqlString, connection);
                    reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        ModelProduct product = new ModelProduct();
                        product.id = reader.GetInt32(0);
                        product.dateCreate = reader.GetDateTime(1);
                        product.name = reader.GetString(2);
                        product.proteins = reader.GetInt32(3);
                        product.fats = reader.GetInt32(4);
                        product.carbohydrates = reader.GetInt32(5);
                        product.kcal = reader.GetInt32(6);
                        listProducts.Add(product);
                    }
                    sqlString = "SELECT eating.id, eating.date_create, eating.product_id, eating.mass, eating.type FROM eating;";
                    command = new OleDbCommand(sqlString, connection);
                    reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        ModelEating modelEating = new ModelEating();
                        modelEating.id = reader.GetInt32(0);
                        modelEating.dateCreate = reader.GetDateTime(1);
                        modelEating.productId = reader.GetInt32(2);
                        modelEating.mass = reader.GetInt32(3);
                        modelEating.type = reader.GetInt32(4);
                        listEating.Add(modelEating);
                    }
                    connection.Close();
                }
                foreach (ModelEating eating in listEating.Where(x => x.dateCreate.ToString("dd.MM.yyyy") == DateTime.Now.ToString("dd.MM.yyyy")))
                {
                    ModelProduct product = listProducts.First(x => x.id == eating.productId);
                    a += product.kcal * eating.mass / 100;
                }

                foreach (ModelEating eating in listEating.Where(x => x.dateCreate.ToString("dd.MM.yyyy") == DateTime.Now.ToString("dd.MM.yyyy") && x.type == 1))
                {
                    ModelProduct product = listProducts.First(x => x.id == eating.productId);
                    breakfast += product.kcal * eating.mass / 100;
                }
                foreach (ModelEating eating in listEating.Where(x => x.dateCreate.ToString("dd.MM.yyyy") == DateTime.Now.ToString("dd.MM.yyyy") && x.type == 2))
                {
                    ModelProduct product = listProducts.First(x => x.id == eating.productId);
                    dinner += product.kcal * eating.mass / 100;
                }
                foreach (ModelEating eating in listEating.Where(x => x.dateCreate.ToString("dd.MM.yyyy") == DateTime.Now.ToString("dd.MM.yyyy") && x.type == 3))
                {
                    ModelProduct product = listProducts.First(x => x.id == eating.productId);
                    supper += product.kcal * eating.mass / 100;
                }
                foreach (ModelEating eating in listEating.Where(x => x.dateCreate.ToString("dd.MM.yyyy") == DateTime.Now.ToString("dd.MM.yyyy") && x.type == 4))
                {
                    ModelProduct product = listProducts.First(x => x.id == eating.productId);
                    snack += product.kcal * eating.mass / 100;
                }
                c = config.targetDown;
                b = c - a;
                LabelStatisticsValue1.Content = a.ToString();
                LabelStatisticsValue2.Content = b.ToString();
                LabelStatisticsValue3.Content = c.ToString();
                LabelBreakfast.Content = breakfast;
                LabelDinner.Content = dinner;
                LabelSupper.Content = supper;
                LabelSnack.Content = snack;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
        }
    }
}
